# Stats Tips

Source repository for the web site [Stats Tips](https://umr-astre.pages.mia.inra.fr/guidelines/statstips/).

## Contributing

You are very welcome to contribute your tips by adding rows to the file [`data/tips.csv`](https://forgemia.inra.fr/umr-astre/guidelines/statstips/-/blob/main/data/tips.csv).

You can also contribute use cases for the data visualisation section.
Just send me an e-mail with your original figure and caption. 
If you include data and code, I will be able to include code in my feedback and be more specific. Otherwise, I will be able to provide some general recommendations.


## Project status

This is work-in-progress. I'm continuously adding and improving the formulation
of tips.



## Authors and acknowledgment

This repository is created and maintained by Facundo Muñoz.

Thanks to Etienne Loire for contributing tips #5 #6 and #7.



<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

